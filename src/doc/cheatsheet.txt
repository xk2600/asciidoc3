AsciiDoc3 cheatsheet v1.0
=========================

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=32]

<<titles,[blue]*Titles Headlines*>>

<<format,[blue]*Formatting Text*>>

<<images,[blue]*Images*>>

<<paragraphs,[blue]*Paragraphs and Admonitions*>>

<<blocks,[blue]*Blocks*>>

<<lists,[blue]*Lists*>>

<<tables,[blue]*Tables*>>

<<formulas,[blue]*Formulas*>>

image:empty.png["empty",height=25]


See the <<end,annotation>> at the of this document. To generate a html-file: 'asciidoc3 -n -a icons -a latexmath cheatsheet.txt'.
Do *not* try 'a2x3 -f pdf cheatsheet.txt', because the plain source yields just an (html-)example and not valid DocBook.
Download a PDF https://asciidoc3.org/cheatsheet.pdf[here]. This document will be updated from time to time.

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

[[titles]]
[blue]*Titles Headlines*


-----------------------
== Level 1
Text here!

=== Level 2
More text.

==== Level 3
More and more text.

===== Level 4
This is the end.
-----------------------


== Level 1
Text here!

=== Level 2
More text.

==== Level 3
More and more text.

===== Level 4
This is the end.

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

-----------------------
Level 1
 ------
Text here!

Level 2
~~~~~~~
More text.

Level 3
^^^^^^^
More and more text.

Level 4
+++++++
This is the end.

-----------------------

Level 1
-------
Text here!

Level 2
~~~~~~~
More text.

Level 3
^^^^^^^
More and more text.

Level 4
+++++++
This is the end.



image:empty.png["empty",height=25]

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]	

[[format]]
[blue]*Formatting Text*

--------------------------

[red]#red text# [yellow-background]#on yellow#
[big]#large# [red yellow-background big]*all bold*
--------------------------

[red]#red text# [yellow-background]#on yellow#
[big]#large# [red yellow-background big]*all bold*



--------------------------
forced +
line break
--------------------------
	
forced +
line break


--------------------------
normal, _italic_, *bold*, +mono+.

``double quoted'', `single quoted'.

normal, ^super^, ~sub~.

--------------------------	

normal, _italic_, *bold*, +mono+.

``double quoted'', `single quoted'.

normal, ^super^, ~sub~.




--------------------------

Command: `ls -al`

+mono *bold*+

`passthru *bold*`
--------------------------
	

Command: `ls -al`

+mono *bold*+

`passthru *bold*`


--------------------------

Path: '/some/filez.txt', '.b'
--------------------------
	
Path: '/some/filez.txt', '.b'


--------------------------
Chars: n__i__**b**++m++[red]##r##

--------------------------	


Chars: n__i__**b**++m++[red]##r##


--------------------------

// Comment
--------------------------

// Comment

empty

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]
	
--------------------------
(C) (R) (TM) -- ... -> <- => <= &#182;

--------------------------	

(C) (R) (TM) -- ... -> <- => <= &#182;

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

--------------------------	
''''
--------------------------	

''''
(Did you notice? The four '''''''' produce a horizontal bar.)

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

image:empty.png["empty",height=25]

--------------------------		
Escaped:
\_italic_, +++_italic_+++,
t\__e__st, +++t__e__st+++,
+++<b>bold</b>+++, $$<b>normal</b>$$
\&#182;
\`not single quoted'
\`\`not double quoted''
--------------------------		


Escaped:
\_italic_, +++_italic_+++,
t\__e__st, +++t__e__st+++,
+++<b>bold</b>+++, $$<b>normal</b>$$
\&#182;
\`not single quoted'
\`\`not double quoted''


image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

------------------
[underline]#Underline text#, [overline]#overline text# and
[blue line-through]*bold blue and line-through*.
------------------

[underline]#Underline text#, [overline]#overline text# and
[blue line-through]*bold blue and line-through*.

image:empty.png["empty",height=25]

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]	


[[images]]
[blue]*Images*

To insert images inline use the image macro:

---------------
and now the tiger image:../images/tiger.png[alt="Tiger",height=35] comes along.

-------------------

and now the tiger image:../images/tiger.png[alt="Tiger",height=35] comes along. Give the absolute or relative path to the source or use the imagedir attribute. The optional align attribute aligns block macro images horizontally. Allowed values are center, left and right. For example:

--------------
image::../images/tiger.png["Tiger image",height=40, align="right"]
--------------

image::../images/tiger.png["Tiger image",height=40, align="right"]

Please look for some additional options in the userguide, section "images".


image:empty.png["empty",height=25]

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]	



[[paragraphs]]
[blue]*Paragraphs and Admonitions*

--------------------
.Optional Title

Usual
paragraph.


------------------

.Optional Title

Usual
paragraph.


------------------
.Optional Title

 Literal paragraph.
  Must be indented.
------------------
	
.Optional Title

 Literal paragraph.
  Must be indented.

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

----------------------
.Optional Title

[source,python]
print('hello again')

This is normal text.

--------------------
	
.Optional Title

[source,python]
print('hello again')

This is normal text.

image:empty.png["empty",height=25]

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

-----------------------------
.Optional Title
NOTE: This is an example
      single-paragraph note.
----------------------------
	
.Optional Title
NOTE: This is an example
      single-paragraph note.


------------------------------
.Optional Title
[NOTE]
This is an example
single-paragraph note.

-----------------------------
	
.Optional Title
[NOTE]
This is an example
single-paragraph note.


------------------

TIP: Tip.
------------------
	
TIP: Tip.



------------------
IMPORTANT: Important.
------------------
	
IMPORTANT: Important.



----------------
WARNING: Warning.
----------------
WARNING: Warning.	


----------------
CAUTION: Caution.
----------------
CAUTION: Caution.

image:empty.png["empty",height=25]

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]


[[blocks]]
[blue]*Blocks*

------------------------------
.Optional Title
 ----                  <1>
*Listing* Block

Use: code or file listings
 ----                  <1>
------------------------------
<1> Start the four - hard against the left margin! The space here is to prevent asciidoc3 from interpreting the - as a nested listing block. 

	
.Optional Title
----
*Listing* Block

Use: code or file listings
----

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]


------------------------
.Optional Title
[source,perl]
 ----              <1>
# *Source* block
# Use: highlight code listings
# (require `source-highlight` or `pygmentize`)
use DBI;
my \$dbh = DBI->connect('...',\$u,\$p)
    or die "connect: \$dbh->errstr";
 ----              <1>
-------------------------
<1> Start the four - hard against the left margin! The space here is to prevent asciidoc3 from interpreting the - as a nested listing block.
// Please note: the above four backslashes \ before the $'s are placed because of the same reason, omit them. 

.Optional Title
[source,perl]
----
# *Source* block
# Use: highlight code listings
# (require `source-highlight` or `pygmentize`)
use DBI;
my $dbh = DBI->connect('...',$u,$p)
    or die "connect: $dbh->errstr";
----

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

---------------------
.Optional Title
****
*Sidebar* Block

Use: sidebar notes :)
****
---------------------
	
.Optional Title
****
*Sidebar* Block

Use: sidebar notes :)
****

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

--------------------------------
.Optional Title
==========================
*Example* Block

Use: examples :)

Default caption "Example:"
can be changed using

 [caption="Custom: "]

before example block.
==========================
---------------------------------


.Optional Title
==========================
*Example* Block

Use: examples :)

Default caption "Example:"
can be changed using

 [caption="Custom: "]

before example block.
==========================



image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]





--------------------------------------
.Optional Title
[NOTE]
===============================
*NOTE* Block

Use: multi-paragraph notes.
===============================

---------------------------------------
	
.Optional Title
[NOTE]
===============================
*NOTE* Block

Use: multi-paragraph notes.
===============================

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

-------------------------------
////
*Comment* block

Use: hide comments
////
-------------------------------
	
(this is empty space, because the coment block is not processed) 

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

---------------------------------
++++
*Passthrough* Block
<p>
Use: backend-specific markup like
<table border="1">
<tr><td>1<td>2</tr>
</table>
++++

---------------------------------


++++
*Passthrough* Block
<p>
Use: backend-specific markup like
<table border="1">
<tr><td>1<td>2</tr>
</table>
++++


image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]


--------------------------
 .Optional Title
 ....
 *Literal* Block

 Use: workaround when literal
 paragraph (indented) like
   1. First.
   2. Second.
 incorrectly processed as list.
 ....
--------------------------
	

 .Optional Title
 ....
 *Literal* Block

 Use: workaround when literal
 paragraph (indented) like
   1. First.
   2. Second.
 incorrectly processed as list.
 ....

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

------------------------------
.Optional Title
[quote, cite author, cite source]
____
*Quote* Block

Use: cite somebody. To be or not to be ...
____

-----------------------------


.Optional Title
[quote, cite author, cite source]
____
*Quote* Block

Use: cite somebody. To be or not to be ...
____


image:empty.png["empty",height=25]

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

[[lists]]
[blue]*Lists*

------------------------------
.Bulleted
* bullet
* bullet
  - bullet
  - bullet
* bullet
** bullet
** bullet
*** bullet
*** bullet
**** bullet
**** bullet
***** bullet
***** bullet
**** bullet
*** bullet
** bullet
* bullet
------------------------------

.Bulleted
* bullet
* bullet
  - bullet
  - bullet
* bullet
** bullet
** bullet
*** bullet
*** bullet
**** bullet
**** bullet
***** bullet
***** bullet
**** bullet
*** bullet
** bullet
* bullet


image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

----------------
.Bulleted 2
- bullet
  * bullet
---------------


.Bulleted 2
- bullet
  * bullet

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

-----------------------------------
[horizontal]
.Labeled horizontal
Term 1:: Definition 1
Term 2:: Definition 2
[horizontal]
    Term 2.1;;
        Definition 2.1
    Term 2.2;;
        Definition 2.2
Term 3::
    Definition 3
Term 4:: Definition 4
[horizontal]
Term 4.1::: Definition 4.1
Term 4.2::: Definition 4.2
[horizontal]
Term 4.2.1:::: Definition 4.2.1
Term 4.2.2:::: Definition 4.2.2
Term 4.3::: Definition 4.3
Term 5:: Definition 5
----------------------------------


[horizontal]
.Labeled horizontal
Term 1:: Definition 1
Term 2:: Definition 2
[horizontal]
    Term 2.1;;
        Definition 2.1
    Term 2.2;;
        Definition 2.2
Term 3::
    Definition 3
Term 4:: Definition 4
[horizontal]
Term 4.1::: Definition 4.1
Term 4.2::: Definition 4.2
[horizontal]
Term 4.2.1:::: Definition 4.2.1
Term 4.2.2:::: Definition 4.2.2
Term 4.3::: Definition 4.3
Term 5:: Definition 5


[TIP]
See more examples in the userguide.

image:empty.png["empty",height=25]

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]


[[tables]]
[blue]*Tables*


---------------------------------------------------------------------
.Title
[width="15%"]
|=======
|1 |2 |A
|3 |4 |B
|5 |6 |C
|=======
---------------------------------------------------------------------


.Title
[width="15%"]
|=======
|1 |2 |A
|3 |4 |B
|5 |6 |C
|=======

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

---------------------------------------------------------------------
.An example table with title, header and footer
[width="40%",frame="topbot",options="header,footer"]
|======================
|Column 1 |Column 2
|1        |Item 1
|2        |Item 2
|3        |Item 3
|6        |Three items
|======================
---------------------------------------------------------------------

.An example table with title, header and footer
[width="40%",frame="topbot",options="header,footer"]
|======================
|Column 1 |Column 2
|1        |Item 1
|2        |Item 2
|3        |Item 3
|6        |Three items
|======================

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]

.Spans, alignments and styles
[cols="e,m,^,>s",width="25%"]
|================
|1 >s|2 |3 |4
^|5 2.2+^.^|6 .3+<.>m|7
^|8
|9 2+>|10
|================

.AsciiDoc3 source
---------------------------------------------------------------------
.Spans, alignments and styles
[cols="e,m,^,>s",width="25%"]
|================
|1 >s|2 |3 |4
^|5 2.2+^.^|6 .3+<.>m|7
^|8
|9 2+>|10
|================
---------------------------------------------------------------------

[TIP]
See more examples here: ./tests/data/newtables(_docbook51).txt



image:empty.png["empty",height=25]

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]


[[formulas]]
[blue]*Formulas*

Some example 'LaTeXMathML' formulas, see './doc/latexmathml.txt' for more infos and the source. And take a look to 'asciimathml.txt' and 'latex-filter.txt' to learn about other ways to produce formulae, also in PDFs. 


- latexmath:[$R_x = 10.0 \times \sin(R_\phi)$]

- latexmath:[$\sum_{n=1}^\infty \frac{1}{2^n}$]

- latexmath:[$\lim_{x\to\infty} f(x) = k \choose r + \frac ab
  \sum_{n=1}^\infty a_n + \displaystyle{ \left\{ \frac{1}{13}
  \sum_{n=1}^\infty b_n \right\} }$]

- latexmath:[$\$\alpha + \$\beta = \$(\alpha + \beta)$]

- latexmath:[$\begin{eqnarray} x & = & \frac{-7 \pm
  \sqrt{49 - 24}}{6} \\ & = & -2 \textrm{ or } -\frac13.
  \end{eqnarray}$]

- latexmath:[$\displaystyle{ V_i = C_0 - C_3
  \frac{C_1\cos(\theta_i+C_3)}{C_4+C_1\cos(\theta_i+C_2)} }$]


.AsciiDoc3 source
image:latexmath_source.png["latexmath_source",height=325]


image:empty.png["empty",height=25]

image:logo_asciidoc3.png["AsciiDoc3 Logo",height=10]


[[end]]
[blue]*Annotation*

This summary of some features of https://asciidoc3.org/[AsciiDoc3] was inspired by the 'Asciidoc Cheat Sheet' from https://powerman.name/doc/asciidoc[powerman]. We added some additional information and a few new 'tricks' ... +
This is only "the tip of the iceberg" - see the userguide ./doc/userguide.txt or https://asciidoc3.org/userguide.html[online] and browse the directories ./doc/-.- and ./tests/data/-.- for many more examples. +
(c) 2023 by Berthold Gehrke berthold.gehrke@gmail.com Given to the Public Domain - if not applicable: MIT license.
